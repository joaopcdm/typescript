import {Spacecraft, Containership} from './base-ships'
import{MillenuimFalcon} from './starfighters'

let ship = new Spacecraft('hyperdrive')
ship.jumpIntoHyperspace()

let falcon = new MillenuimFalcon()
falcon.jumpIntoHyperspace()

let goodForTheJob = ( ship: Containership) => ship.cargoContainers > 2
console.log(`Is falcon good for the job? ${goodForTheJob (falcon) ? 'Yes':'No'}`)
